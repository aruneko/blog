FROM jojomi/hugo AS build
WORKDIR /tmp
COPY . .
RUN hugo -t blackburn

FROM nginx:alpine
COPY --from=build /tmp/public /usr/share/nginx/html
