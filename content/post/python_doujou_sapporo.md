+++
date = "2016-07-31T12:28:39+09:00"
draft = false
title = "Python道場 Sapporoに参加してきた"
tags  = ["せきゅぽろ"]
+++

2016年7月30日にインフィニットループ会議室で開催された「Python道場」に参加してきました。

今回はPython2でネットワークプログラミングを行ったあと、脆弱性のある掲示板プログラムに対してXSSを仕掛ける演習を行いました。

講師はトレンドマイクロの新井悠さんでした。

まずはXSSの種類(反射型/蓄積型)を紹介していただき、今回は蓄積型のXSSについて扱うと言うことを教えていただきました。

そしてサンプルの脆弱性のある掲示板プログラムをいただいて早速実行。

続いて、Pythonのsocketライブラリを使ってHTTP通信を行うプログラムを書きました。socketライブラリは、通信の方式なども切り替えられるようになっており、TCPだけでなくUDPなどなどのパケットも扱えるとのことでした。

こんな感じのコードを書いてみました。生のHTTPを書いたのは久々。掲示板に対して繋いでみるだけのお手軽プログラムです。

```python
#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import socket
import time

target_host = "127.0.0.1"
target_port = 8000

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((target_host, target_port))

client.send("GET / HTTP/1.1\r\nHost: 127.0.0.1\r\n\r\n")
time.sleep(2)
response = client.recv(4096)

print response
```

これは面倒と言うことで、続いてはurllibを使ってみるサンプルです。非常に簡単。

```python
#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import urllib

url = "http://127.0.0.1:8000/cgi-bin/index.py"

page = urllib.urlopen(url)
source_code = page.read()
print source_code
```

引き続いてBeautifulSoupを使ってWebスクレイピングをしてみるテストをしてから、mechanizeによるWebページの操作を学びました。

ここからが本番。サンプルの掲示板に対して蓄積型XSSを仕掛けてみるコードを書いてみようとのことでした。

もらったサンプル掲示板のコードを読んだところ、名前欄にもテキスト欄にもエスケープ処理が施されていないことがわかったので、BeautifulSoupで引っこ抜いてきたinputタグやらtextareaタグに対してMechanizeを使ってJavascriptでalertを出すプログラムを注入してみたところ、攻撃を成功させることができました。

なにやら一番最初に完成したらしく、景品として新井さんが翻訳を担当した本である「サイバーセキュリティプログラミング」をいただいてしまいました(せっかくならサイン本にしていただこうかとも思ったのですがいかんせん筆記用具を持っておらず...)。

![サイバーセキュリティプログラミング](/images/IMG_0229.JPG)

途中で謎の頭痛と激しい吐き気に見舞われるなどハプニングにも遭遇しましたが、なんとか無事に執り行うことができて良かったと思います。

最後に、このイベントを企画・運営してくださったせきゅぽろとLOCALの皆さん、トレンドマイクロからいらっしゃってくださった新井さん、そして参加者の皆さんに感謝です！

---

今回は、LOCAL交通費支援制度を利用してこのイベントになんとか参加することができました。ご支援いただき本当にありがとうございました。
