+++
date = "2022-05-01T00:00:00+09:00"
draft = false
title = "合同会社 Georepublic Japan を退職しました"
tags = ["その他"]
+++

タイトルにもあるとおり、2022年 4月末日をもって新卒だった 2019年4月1日からおよそ3年間勤めてきた合同会社 Georepublic Japan を退職しました。社長の関さんをはじめ、関係する皆さんには大変お世話になりました。改めてお礼を申し上げます。そういうわけで、この記事では在職期間を振り返っていきたいと思います。

Georepublic という会社は地理空間情報を用いたソフトウェアの開発を専門とする会社です。東京の虎ノ門と神戸にオフィスがあり、僕はなんやかんやあって神戸のオフィスで働いていました。同級生の皆さんは大学院の卒業後北海道を離れてほとんどが東京か札幌に就職する中、一人関西へ乗り込んでいった一抹の不安や寂しさを今でも思い出します。

「なんやかんや」というのは、当時の神戸オフィスには外国人社員が多く勤めており、英語を使わざるを得ない環境に身を置くことで英語のスキルを伸ばせるのではないかというもくろみがあったためです。また、どうせいつか東京に行くことになるだろうから、そうなる前に東京ではないところに住んでおくという経験をしてみたかったという理由もあります。

入社したはいいものの、ベンチャーにありがちなことかもしれませんが研修制度などは全く整っていなかったため、いきなりプロジェクトに投入されたり、（もくろみ通り）英語で喋らざるを得なかったりしてそれなりに忙しい日々を過ごし始めたと思います。それと同時にオフィスを使って勉強会を開いてみたり、Scala 関西や Code for Kobe など関西近辺の集まりにも顔を出すなど、色々となじもうとして必死だったのかと思います。そのあと炎上案件に巻き込まれたのは良い思い出でしたね。

入社するきっかけでもあり、メインで関わることになった [My City Report](https://www.mycityreport.jp/) 案件に首を突っ込み始めたのも割と入社してそんなに経っていない時期だったと思います。ちなみにこのウェブサイトも現行のスマホアプリもベースとなる部分は僕が作ったものです。参加自治体にお住まいの皆さんはぜひ使ってみてください。なお振り返ってコードを見るとひどいものですね。引き継いだ皆さんにはご迷惑おかけします......

こんな感じでのらりくらりとやっていけるのかなと思っていました。コロナが来るまでは。

コロナ禍になってからはもちろん出社することも少なくなりましたし、勉強会の類いも一切開催されなくなったので、アクティブな引きこもりみたいな生活を送っていたと今振り返ってみて思います。コロナ禍になる前は数週間に一度ぐらいのペースで全国を飛び回っては登壇（+懇親会）だのなんだのをしていたのが、せいぜい私鉄に乗って関西近辺の有名観光地に一人で行って返ってくるぐらいのことしかやらなくなってしまっていたなと。

このコロナの状況でやはり強く感じたのが圧倒的なコミュニケーション量の不足でした。特に Georepublic では誰にとっても非母国語でのコミュニケーションも挟む必要があるため、なおのこと情報量が欠落するオンラインだけのコミュニケーションは、物事を円滑に進めるという観点から見ると難易度が高かったなと感じていたというのが正直なところだったりします。コミュニケーション面は結局どうすればうまく行くのかという試行錯誤も含めて、在職中にあまり工夫できなかったのは心残りだと思っています。このあたりは次のお仕事でも常に考えていきたいですね。

さて、そんなこんなで次のお仕事は東京でセキュリティ屋さんです。地図屋さん(?)からの転職なので徐々に慣れていければなと。あるねこ先生の次回作にご期待ください。

また、東京近辺のみなさまはようやく気軽に足を運べるようになりましたので、もろもろお誘いいただければ喜んで参上したいと！
