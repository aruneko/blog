+++
date = "2016-05-12T16:35:16+09:00"
draft = false
title = "セキュリティキャンプ応募用紙について"
tags = ["セキュリティキャンプ"]
+++

なんか応募用紙を晒すのが流行っているようなので、便乗します。他の人のも見ていますが、ことごとく僕のが雑魚過ぎて大変つらみを感じております。と、つべこべ言ってないで晒しますね。共通問題とIPアドレスの一部は加工しますが悪しからず。あと大なり小なりがうまく表示されてないけどごめんね。

<h3>共通問題</h3>

下記の共通問題にはすべて回答してください。

■ 共通問題1
セキュリティ・キャンプに応募した自分なりの理由とセキュリティ・キャンプで学んだことを何に役立てたいかを教えてください。
【以下に回答してください（行は適宜追加してください）】
自分の野望を素直に書きました。色々なことを知りたいということと、それを伝える活動をしたいということですね。
【回答ここまで】

■ 共通問題2
セキュリティに関することで、過去に自分が経験したことや、ニュースなどで知ったことの中から、最も印象に残っていることを教えてください。また、その印象に残った理由も教えてください。
【以下に回答してください（行は適宜追加してください）】
VPSを運営していて、ShellShock脆弱性を突いた攻撃が行われたことを説明しました。
【回答ここまで】

■ 共通問題3
その他に自己アピールしたいことがあれば自由に書いてください。（たとえば、あなたが希望する講座を受講する上で、どのような技術力を持っているか、部活動、技術ブログ、GitHub、ソフトウェア開発、プログラミングコンテスト、勉強会での発表・運営などの実績や熱意があれば、あるだけ書いてください。）
【以下に回答してください（行は適宜追加してください）】
まぁここは最近勉強会の運営に熱心になっていることとか、今回のキャンプでその知識を活かしてまた勉強会を開いていきたいとかそういうことを書いて送りました。また、近所でなかなか勉強会が開かれないこととかそういう指摘もしてみたり。
【回答ここまで】

<h3>選択問題</h3>

下記の選択問題の中から 5つ 選択して回答してください。
なお、回答した問題は、冒頭の□を■にしてください。

■選択問題4　（左側の□について、回答した問題は■にしてください）
通信やプロトコルに関する問題です。次の1-3の問いに回答してください。ただし3は任意とします。
(1)インターネット上で自分が興味を持ったサイト(又はホスト)を見つけ、 自分の端末からそのサイトへ ping を打ちなさい。その結果とそのサイトに興味を持った理由を記述してください
(2)自分の端末から1のサイトの間のネットワーク通信が、技術的にどのような仕組みで何が行われているのか考察して記述してください（使用する通信は ping に限定しません）
(3)可能であれば、自分の端末から1のサイトの間で、どのようなプロトコルを使ってどういったサービスが実現できているのか、また将来どんな事が実現できれば良いだろうか、考察して記述してください

【以下に回答してください（行は適宜追加してください）】

(1)
自分が最も興味を持ったサイトは「自分のサイト」すなわち「aruneko.net」である。お名前.comのVPSで運用しているが、
自宅などから実際どんな経路を辿って自分のサイトまでたどり着いているのか、その背景を知りたくなった。
自分が運用しているサイトなのに、その背景を全く知らないまま漫然と過ごしてしまったが、この機会にしっかりと理解したいと思う。
以下にWindowsで実行したpingの結果を示す。

<pre><code>----------
aruneko.net [157.7.209.36]に ping を送信しています 32 バイトのデータ:
157.7.209.36 からの応答: バイト数 =32 時間 =45ms TTL=52
157.7.209.36 からの応答: バイト数 =32 時間 =44ms TTL=52
157.7.209.36 からの応答: バイト数 =32 時間 =43ms TTL=52
157.7.209.36 からの応答: バイト数 =32 時間 =44ms TTL=52

157.7.209.36 の ping 統計:
    パケット数: 送信 = 4、受信 = 4、損失 = 0 (0% の損失)、
ラウンド トリップの概算時間 (ミリ秒):
    最小 = 43ms、最大 = 45ms、平均 = 44ms
----------
</code></pre>

(2)
ブラウザでaruneko.netにアクセスしたと想定する。
この場合はHTTP通信が行われ、HTTPリクエストが自分のサーバーまで届くこととなる。
まずブラウザで生成されたHTTPリクエスト通常TCP通信が行われるため、
HTTPリクエストが載っているパケットにはTCPヘッダが取り付けられる。
このヘッダによって、自分のマシンのポートからサーバーの80番ポートまで届けるための「目印」を取り付けることができる。

次にIPヘッダの取り付けが行われるが、「aruneko.net」だけだと相手サーバーのIPアドレスが分からない。
ここでDNSが登場する。まず家のルーターにあるDNSキャッシュにこのドメイン名を問い合わせ、
対応するIPアドレスがあるかどうかを確認する。見つからなかった場合は上位のDNSに問い合わせる。
今回はnslookupコマンドを使って調査したが、どうやら自宅ルーターのDNSキャッシュに残っていたことが判明した。以下が実行結果である。

<pre><code><br />------
サーバー:  local.gateway
Address:  192.168.0.1

権限のない回答:
名前:    aruneko.net
Address:  157.7.209.36
------
</code></pre>

これによってaruneko.netのIPアドレス「157.7.209.36」が取得できた。
自分のグローバルIPアドレスと送信先のIPアドレスを内容とするIPヘッダがこれでようやく付加されたことになる。
続いて、Ethernet接続で、各機器の間をパケットが流れていく。まずどのような機器を通ったかをtracerouteコマンドで確認する。

<pre><code><br />------

% traceroute -I -a aruneko.net
traceroute to aruneko.net (157.7.209.36), 64 hops max, 72 byte packets
 1  [AS56220] local.gateway (192.168.0.1)  2.802 ms  1.039 ms  1.011 ms
 2  [AS4713] xxx.xxx.xxx.xxx (xxx.xxx.xxx.xxx)  39.053 ms  37.572 ms  33.178 ms
 3  [AS4713] xxx.xxx.xxx.xxx (xxx.xxx.xxx.xxx)  47.718 ms  44.439 ms  29.045 ms
 4  [AS4713] xxx.xxx.xxx.xxx (xxx.xxx.xxx.xxx)  42.437 ms  33.815 ms  49.081 ms
 5  [AS4713] xxx.xxx.xxx.xxx (xxx.xxx.xxx.xxx)  41.663 ms  47.610 ms  42.709 ms
 6  [AS4713] xxx.xxx.xxx.xxx (xxx.xxx.xxx.xxx)  44.772 ms  43.425 ms  41.707 ms
 7  [AS4713] xxx.xxx.xxx.xxx (xxx.xxx.xxx.xxx)  43.678 ms  44.178 ms  44.339 ms
 8  [AS4713] xxx.xxx.xxx.xxx (xxx.xxx.xxx.xxx)  47.821 ms  52.603 ms  49.801 ms
 9  [AS4713] xxx.xxx.xxx.xxx (xxx.xxx.xxx.xxx)  66.244 ms  47.877 ms  41.979 ms
10  [AS2914] ae-11.r24.tokyjp05.jp.bb.gin.ntt.net (61.213.162.165)  46.890 ms
    [AS2914] ae-11.r25.tokyjp05.jp.bb.gin.ntt.net (61.213.162.173)  40.479 ms  48.848 ms
11  [AS2914] ae-2.r00.tokyjp05.jp.bb.gin.ntt.net (129.250.2.23)  50.838 ms
    [AS2914] ae-1.r00.tokyjp05.jp.bb.gin.ntt.net (129.250.2.21)  42.071 ms  42.816 ms
12  [AS2914] xe-0-2-0-32.r00.tokyjp05.jp.ce.gin.ntt.net (203.105.72.230)  41.925 ms  56.305 ms  44.986 ms
13  [AS7506] g-o-4eb-a13-3-v-2104.interq.or.jp (157.7.40.138)  44.673 ms  45.281 ms  48.379 ms
14  [AS7506] vkvm1-4ee-a01-1-e-1-49.interq.or.jp (157.7.40.114)  53.627 ms  43.721 ms  40.080 ms
15  [AS7506] v157-7-209-36.myvps.jp (157.7.209.36)  45.198 ms  43.570 ms  42.649 ms

------
</code></pre>

14の機器を通過して、やっとブラウザが発したパケットが自分のサーバーへと届いたことが分かる。

この機器同士の通信時に、お互いのMACアドレスが用いられる。
Ethernetヘッダに送信元と送信先のMACアドレスが載せられ、機器のやりとりごとにEthernetヘッダは付け替えられる。
さらにこの情報には「AS番号」が付加されている。家の中のネットワークである[AS56220]とxxx回線を示す[AS4713]、
NTT回線を示す[AS2914]にお名前.comのネットワークを示す[AS7506]の4つに大きく分けることができる。

自分のコンピュータから出たパケットは、まず無線LANを通じて無線LAN親機まで届く。この間は「IEEE 802.11n」で送信される。
無線LAN親機はブリッジモードのため、パケットはそこを素通りし、ルーター(兼モデム)へとたどり着く。
その間には「ギガビットイーサネット」が使われており、「1000BASE-T」規格のLANケーブル(カテゴリ6)で結ばれている。
ルーターから先はxxxのADSL回線となっている。これは光ではなくツイストペアケーブル通信線を用いる方式だ。
Accaの回線より先は、どのような規格になっているかわからないが、少なくとも自分のサーバーの直前では光回線になっていると考えられる。
さらに、このサーバーは仮想サーバーのため、サーバーにたどり着くために仮想的なネットワークアダプタが用いられていると推測できる。

こうしてサーバーに届いたパケットは、EthernetヘッダやIPヘッダがはずされる。TCPヘッダのポート情報を読み取り、
いよいよ届けられたHTTPパケットはそのポートを通ってApacheへ届く。
Apacheが処理したレスポンスは再びパケットに分割され、TCPヘッダやIPヘッダが付加される。
今度は送信されてきたIPアドレスとポートに向けてそれらの情報が取り付けられる。

先ほどとは逆の手順で自宅ルーターまでやってきたパケットは、NAPTに迎えられる。
家の中にはたくさんの機器がネットワークに繋がっており、グローバルアドレスだけではどの機器のポートに
そのパケットを届ければよいかわからない。そのためNAPTによって、パケットを送信したマシンに紐付けられたポートを目指して
ローカルIPアドレスを逆検索し、そのIPが振られたマシンのそのポートに対してパケットを送信する。

そしてようやくマシンに戻ってきたパケットはブラウザまで届き、HTTPレスポンスとして解釈され、
それに則ってHTMLやCSS、Javascriptが解釈されて表示される。
これが、自宅と自分のサーバー間で行われた通信の流れとなる。

(3)
例えば、無線LAN規格である「IEEE 802.11」系統の規格は、各地の無線LANスポットで活用が広がっている。
ただセキュリティの問題や導入にあたっての費用の問題、接続距離の問題も立ちはだかっている。
同一アクセスポイントで同じ暗号化キーを用いているのであれば、それは暗号化していないのも同然である。

もし同一アクセスポイントでも、接続相手ごとにワンタイムパスワードを発行してそれぞれ違う暗号化キーで
接続できるようなプロトコルができれば、公共の場でも簡単かつ安心に無線LANに接続できるサービスが提供できる。

また、山間部などどうしても近距離にアクセスポイントを設置できない場合もある。
遠距離であっても安定して通信できるような誤り訂正技術や圧縮技術が登場すれば、
低コストで無線LANサービスを提供することにも繋がるだろう。そうなれば、平時だけでなく災害時を含め
様々な場面で無線LANを活用できる。特に災害時には電話回線が逼迫することもままあるため、
このような技術を使ってインターネット経由でやりとりできれば、本当に電話回線が必要なところに
行き届くことにも繋がると考えている。

【回答ここまで】

■ 選択問題5　（左側の□について、回答した問題は■にしてください）
以下のようなC言語の関数functionがあるとします。

```c
void function(int *array, int n) {
  int i;
  for(i = 0; i &lt; n; i++) {
    array[i] = i * n;
  }
}
```

上記プログラムをコンパイルした結果の一例 (i386)は以下となりました。

<pre><code>00000000 &lt;function&gt;:
   0:   56                   push   %esi
   1:   53                   push   %ebx
   2:   8b 5c 24 0c          mov    0xc(%esp),%ebx
   6:   8b 4c 24 10          mov    0x10(%esp),%ecx
   a:   85 c9                test   %ecx,%ecx
   c:   7e 18                jle    26 &lt;function+0x26&gt;
   e:   89 ce                mov    %ecx,%esi
  10:   ba 00 00 00 00       mov    $0x0,%edx
  15:   b8 00 00 00 00       mov    $0x0,%eax
  1a:   89 14 83             mov    %edx,(%ebx,%eax,4)
  1d:   83 c0 01             add    $0x1,%eax
  20:   01 f2                add    %esi,%edx
  22:   39 c8                cmp    %ecx,%eax
  24:   75 f4                jne    1a &lt;function+0x1a&gt;
  26:   5b                   pop    %ebx
  27:   5e                   pop    %esi
  28:   c3                   ret  
</code></pre>

このとき以下の(1)～(5)の設問について、回答と好きなだけ深い考察を記述してください。知らない点は、調査したり自分で想像して書いてもらっても結構です。どうしてもわからない部分は、具体的にここがわかりませんと記述しても良いです。(1)～(2)の回答は必ず答えてください。(3)～(5)の回答は任意です。わかることを書いてください。CPU やコンパイラは特定の実装を例に説明しても良いですし、理想を自由に考えても良いです。

(1)【必須】上記の C 言語のプログラムはどのような動作をしますか。また、この関数を呼び出して利用する main 関数の例を作成してください。
(2)【必須】上記のアセンブリコードを、いくつかのブロックに分割して、おおまかに何をしている部分かを説明してください。もし、上記のアセンブリが気に入らないのであれば、好きなアーキテクチャやコンパイラのアセンブル結果を載せて説明しても良いです。
(3)【任意】 コンパイラがソースコードの関数を解釈して、ターゲットのアーキテクチャのバイナリを生成するまで、どのように内部で処理を行っていると思いますか。（キーワード: 構文解析、変数、引数、呼出規約、レジスタ、スタック、アセンブラ、命令セット）
(4)【任意】CPU の内部では、プログラムのバイナリはどのように解釈され実行されていると思いますか。（キーワード: フェッチ、デコード、オペコード、オペランド、命令パイプライン、回路）
(5)【任意】現在の CPU やコンパイラの不満点があれば自由に記述してください。

【以下に回答してください（行は適宜追加してください）】

(1)
このプログラムはint型配列の先頭アドレスとそのサイズnを渡して、配列添字(0～n-1)のn倍をその配列の先頭要素からに順番に詰めるプログラムとなっている。以下にこれを用いたmain関数の例を示す。

```c
int main(void) {
    // 要素数10のint型配列を確保
    int arr[10] = {};
    // 配列の先頭アドレスとそのサイズを引数にとって呼ぶ
    function(arr, sizeof(arr)/sizeof(int));
    // for文で一つずつの要素を走査しながら表示
    // 配列の長さは何度も計算する必要が無いので、一回だけ計算するように工夫
    int i, l;
    for (i = 0, l = sizeof(arr)/sizeof(int); i &lt; l; i++) {
        printf("%dn", arr[i]);
    }
    return 0;
}
```

(2)
[1]
これは関数定義を示している

<pre><code>00000000 &lt;function&gt;:
</code></pre>

[2]
ここではesiおよびebxレジスタのスタックへの待避を行っている。
この2つのレジスタは関数から戻ってきた際に元の状態に復元されなければならない。

<pre><code>   0:   56                   push   %esi
   1:   53                   push   %ebx
</code></pre>

[3]
ここではまずebxレジスタに[esp+0xc]が指すアドレス(スタック領域の一部)にある値(arrayのアドレス)をコピーし、
次にecxレジスタに[esp+0x10]が指すアドレス(スタック領域の一部)にある値(nの値)をコピーしている。
続いてecxレジスタの値と0を比較し、もしecxレジスタの値が0であれば、「26:5b 」の行までジャンプする。
そうで無い場合はここを無視して次の命令へと進む。

<pre><code>   2:   8b 5c 24 0c          mov    0xc(%esp),%ebx
   6:   8b 4c 24 10          mov    0x10(%esp),%ecx
   a:   85 c9                test   %ecx,%ecx
   c:   7e 18                jle    26 &lt;function+0x26&gt;
</code></pre>

[4]
ここではまずecxレジスタの値(nの値)をesiレジスタにコピーしている。続いてedxレジスタ(i<em>nの値)とeaxレジスタ(iの値)に0を書き込む。
ここでedxレジスタの値を[ebx + eax * 4]が指すメモリ上の位置(array[i]のアドレス)に書き込む。
さらにeaxレジスタに1を加え(i++)、edxレジスタにesiの値(nの値)を加える(結果的にi</em>nを計算したこととなる)。
そしてecxレジスタの値とeaxレジスタの値を比較し、もし等しくなければ「1a: 89 14 83」の行までジャンプする。
そうでなければここを無視して次の命令へと進む。

<pre><code>   e:   89 ce                mov    %ecx,%esi
  10:   ba 00 00 00 00       mov    $0x0,%edx
  15:   b8 00 00 00 00       mov    $0x0,%eax
  1a:   89 14 83             mov    %edx,(%ebx,%eax,4)
  1d:   83 c0 01             add    $0x1,%eax
  20:   01 f2                add    %esi,%edx
  22:   39 c8                cmp    %ecx,%eax
  24:   75 f4                jne    1a &lt;function+0x1a&gt;
</code></pre>

[5]
ここでは、先ほど待避したesiおよびebxレジスタの値をスタックから書き戻している。
スタックは片方からしか出し入れできないため、格納した順番と逆の順で書き戻しを行う。
そして最後に呼ばれた元の関数に処理を戻す。

<pre><code>  26:   5b                   pop    %ebx
  27:   5e                   pop    %esi
  28:   c3                   ret  
</code></pre>

(3)
まずコンパイラは字句解析を行う。これによってソースコードはプログラム言語仕様に沿った最小単位(数値や記号、変数など)になるようプログラムが分割され、
その一覧がリストとなって生成される。これを特にトークンリストと呼ぶ。
C言語では、ここでプリプロセッサによるマクロ展開などの置換処理が行われる。
仮にここで言語仕様に沿わない字句が検出されるとコンパイルは停止する。
次に構文解析が行われる。ここではトークンリストを読み取って構文木が作成される。
構文木には、その言語の文法に従って作成された木構造が作られる。
もし文法に従わない部分があった場合、構文木の構築に失敗してコンパイルも停止する。
次にコンパイラは意味解析を行う。構文木を分析して型や関数・変数の定義をチェックする。
ここでも意味が解析できなくなるとコンパイルが停止する。
意味解析が行われた構文木に対して、引き続いて中間コード生成が行われ、さらに最適化が行われる。
ここに来てようやくアセンブリ言語に変換される。ここでは使用される機械語や
レジスタの使用方法・順序に至るまで効率的になるように変換される。
また、ターゲットのCPUやビット数ごとに解釈できる命令セットは異なるため、
どのターゲットに向けた機械語を吐くべきかについても考慮される。
その上、OSによっても機会語の使われ方が異なるため、この点についても考慮されなければならない。
そしてこのとき、呼び出し規約が活用される。例えば関数などの命名法や引数の渡し方、戻り値の返し方などである。
例えばC言語で言うと、「int f(int a, int b, int c)」のような関数があった場合、その関数名をアセンブリではどう命名するかとか、
引数はスタックに積まれるために「c, b, a」の順で渡されるとか、
戻り値は特定のレジスタに必ず格納するとか、そういったことが決められている。
アセンブリ言語に変換されたプログラムはアセンブラを使って機械語に翻訳され、
動作に必要なライブラリとリンクされた後にようやく実行可能バイナリが生成される。

(4)
CPUはまずメモリ上に展開されたプログラムに対して命令コードを取り出すフェッチ作業を行う。
この取り出された命令はCPU内部のレジスタに移される。
レジスタに移った命令はここでデコードされる。これは01の連なりからCPUに対する命令を解釈する作業である。
命令であるオペコードやその命令の対象となるオペランドが明確に区別され、実行へと移されていく。
一方、今までの作業を1命令ごとに繰り返すとなると、各作業を担当する部分が使われない時間が生じてくる。
これは無駄であるため、空き時間が生じないようにある命令のフェッチが終わったらデコードを待たずに次のフェッチを受け入れ...
と言った風に次々と命令を並列的に受け付ける。これを命令パイプラインと呼ぶ。
このように解釈された機械語は最終的に電圧の高低となってCPU内部の論理回路へと流される。
命令によって様々な回路が準備されているが、それらは選択回路によって切り替えられ、適切な命令が実行できるようになっている。

(5)
最近関数型言語(Haskell)を学び始めたのであるが、現在のCPUだとこれをそのまま解釈することはできない。
そのため、コンパイルを行う際に結局手続き型へと置き換えられてしまう。

確かに抽象度が高く、その裏がどんな手続きになっているのかを知る必要がない関数型言語は
機械語を深く知らないプログラマにとっては扱いやすい一面も持ち合わせている。
一方で、それを現在のCPUアーキテクチャに対応した機械語に翻訳する作業はとても重たい作業である。
実際、同じようなことをするプログラムでも、Haskellで書いたものよりCで書いたものの方が
圧倒的にコンパイル作業は早く終わってしまうし、実行速度もCの方が優れている場合がままある。

空想ではあるが、もし関数型言語をそのまま解釈できるような命令セットを持ったCPUがあれば、
とても面白いのではないかと想像した。

【回答ここまで】

■選択問題8　（左側の□について、回答した問題は■にしてください）
gccが持つ-fno-stack-protectorは、どのようなセキュリティ機能を無効にするオプションであるのか、またこの機能により、どういった脆弱性からソフトウェアを守れるのかをそれぞれ記述してください。

【以下に回答してください（行は適宜追加してください）】
このオプションはスタックオーバーフローを検知する機能を無効にするものである。
このオプションはデフォルトで有効であり、スタック上の変数がオーバーフローして
関数の戻り先アドレスが書き換えられてしまったことを検知することができる。
このオプションが有効になっていると、攻撃者があえてスタックオーバーフローを起こして
関数の戻り先アドレスを書き換えることで、その先に忍ばせた任意コードを実行させることを
未然に検知してプログラムを終了させることができる。
このスタックオーバーフローの検知のために、スタックにローカル変数を積む前に
「カナリア」と呼ばれる緩衝領域を設ける。ここの値を事前に記録しておき、
もし関数の戻り先アドレスを参照する際にここの値が書き換わっていたら
スタックオーバーフローが発生したものと見なし、プログラムを停止させる。

【回答ここまで】

■選択問題9　（左側の□について、回答した問題は■にしてください）
以下のコードは、与えられたテキスト内からURLらしき文字列を探して、それらを&lt;a&#62;要素でリンクにしたHTMLを生成するJavaScriptの関数であるとします。攻撃者が引数 text の中身を自由に制御可能な場合、このコードにはどのような問題点があるか、またこのコードを修正するとすればどのようにすればよいか、自分なりに考察して書いてください。

```javascript
function makeUrlLinks( text ){
 var html = text.replace( /[w]+://[w.-]+/[^rn t&lt;&gt;"']*/g, function( url ){
   return "&lt;a href=" + url + "&gt;" + url + "&lt;/a&gt;";
   } );
 document.getElementById( "output" ).innerHTML = html;
}
```

【以下に回答してください（行は適宜追加してください）】

この正規表現では、HTMLタグに使われる文字を弾く作業を、「http://hoge.com/fuga」 を例にすると、fugaの部分のみにしか導入していない。
そのため「hoge.com」部分に任意のHTMLを埋め込むことができてしまう。例えば

<pre><code>http://&lt;a href="http://bar.com"&gt;aruneko.net/
</code></pre>

と入力すると

<pre><code>http://&lt;a href="http://bar.com"&gt;aruneko.net/&lt;/a&gt;
</code></pre>

という結果が返ってきてしまう。リンク先のURIを注視しない人は安易に
偽サイトに飛ばされてしまう可能性が考えられる。他にも、scrptタグを忍ばせれば、
任意のJavascriptも実行できてしまうだろう。
これを解決するために、予めHTMLに関連する部分を取り去ってしまうのが良いと考えられる。
具体的には内部に埋め込まれたHTML部分を空文字で置き換えてから、
その後の処理を行うと安全になるのではないだろうか。以下のようなコードを考えた。

```javascript
function makeUrlLinks( text ){
 var tmp = text.replace(/&lt;.*&gt;/g, "");
 var html = tmp.replace( /[w]+://[w.-]+/[^rn t&lt;&gt;"']*/g, function( url ){
   return "&lt;a href=" + url + "&gt;" + url + "&lt;/a&gt;";
   } );
 document.getElementById( "output" ).innerHTML = html;
}
------
```

2行目にHTMLとおぼしき部分を丸ごと取り去ることができるように正規表現を設定している。
その上で、処理済みの文字列に対してURIを抽出、表示している。

【回答ここまで】

■選択問題11　（左側の□について、回答した問題は■にしてください）
下記バイナリを解析し、判明した情報を自由に記述してください

<pre><code>------------------------------------------------
D4 C3 B2 A1 02 00 04 00 00 00 00 00 00 00 00 00 
00 00 04 00 01 00 00 00 88 EB 40 54 A2 BE 09 00 
52 00 00 00 52 00 00 00 22 22 22 22 22 22 11 11 
11 11 11 11 08 00 45 00 00 44 1A BD 40 00 80 06 
3A 24 C0 A8 92 01 C0 A8 92 80 10 26 01 BB 86 14 
7E 80 08 B3 C8 21 50 18 00 FC 0D 0E 00 00 18 03 
03 00 17 01 0E FB 06 F6 CD A3 69 DC CA 0B 99 FF 
1D 26 09 E1 52 8F 71 77 45 FA
------------------------------------------------
</code></pre>

【以下に回答してください（行は適宜追加してください）】

まずこのバイナリを解析するにあたって、これ自身が一体何のバイナリなのかを調べる必要がある。
そこで先頭4バイトに注目し、これで検索を行った。
するとこのバイナリはpcap(キャプチャしたパケットの記録)ファイルであることが判明した。
先頭4バイトに絞った理由はただの直感である。
ただ、不明なバイナリを解析する際には先頭数バイトをまず調べると良い
という点については以前の勉強会で習っていたためであるとも言える。

このバイナリの正体が分かったところで、pcapファイルを解析するためにWiresharkを用いることに決めた。
このバイナリをStirlingで入力してpcapファイル化し、Wiresharkで解析を行った。
まずEthernetヘッダを確認した。これによると、この通信はEthernetII形式で行われていることが判明した。
送信元のMACアドレスは「11:11:11:11:11:11」で、受信側のMACアドレスは「22:22:22:22:22:22」であった。
さらにこの通信はIPv4方式で行われることも判明した。

このことより、Ethernetヘッダの構造も分かってきた。
先頭6バイトが送信元MACアドレス、次の6バイトが送信先MACアドレス、
そして最後の2バイトがこれに続くヘッダの種類を表している。
従ってこの先のバイト列は、IPv4ヘッダとなっていることが確実となった。
IPv4ヘッダを表す数は「0x0800」としてEthernetIIで定義されていることも確認できた。
蛇足ではあるが、IPv6は「0x86dd」として定義されている。

続いてIPヘッダを解析する。先頭4bitが「4」となっており、
このヘッダがIPv4のヘッダであることを明らかにしている。
続いての4bitがヘッダ長を表しており、ここでは「5」と書かれているが、
この数は4バイトがいくつ分かを示しているためこのヘッダは全体で
4バイト * 5個 = 20バイト
分あることを示している。続いての1バイトでDifferentiated Services(まとまりを持った通信サービス保証)
の取り決めを行っているが、今回は指定されていないことが分かる。
続く2バイトはIPパケット長を表しており、IPヘッダからこのパケット末尾までの長さを示している。
今回は68バイトあることが分かった。
次の2バイトは分割されたパケットを再構築する際に他のパケットと結合されないようにするためのIDを表している。
今回は0x1abdが割り当てられており、これと同様のIDを持ったパケットと後に再構築されることとなる。
続いての3bitはこのパケットに関するフラグが収められている。
最初の1bitは使用されておらず、次の1bitがパケット分割の禁止・許可を表し、
最後の1bitが断片化された場合の末尾のパケットかどうかを表す。
今回は2bit目が1、3bit目が0となっているため、断片かを禁止しており、これが末尾のパケットであることを示している。
これに続く13bitがパケットが分割された際にこのパケットが何番目に来るかを示している。
しかしこのパケットは1個で完結しているため、0が指定されている。
続いての1バイトはこのパケットの存在時間を示している。
このパケットが中継される度にこの数値が一つずつ減っていき、0になると破棄される。
これによってもし回線にループが起きていたとしても一定の回数で破棄されるため、無限に回り続けることはない。
今回は128回と指定されている。続いての1バイトはこのヘッダに続く部分のプロトコルを示している。
今回は6と指定されており、調べたところこれはtcpを示していることが分かった。
また蛇足となるがtcpのほかによく使われるUDPは17が割り当てられている。
次の2バイトはこのIPヘッダ自体の正しさを証明するためのチェックサム部分となっている。
今回このチェックサムをWiresharkに計算してもらったところ、
チェックサムと一致するIPヘッダであることが判明した。
続いての8bitは前4bitが送信元IPアドレス、後4bitが送信先IPアドレスを表している。
今回は送信元IPアドレスが「192.168.146.1」で、送信先IPアドレスが「192.168.146.128」であることが分かった。

IPヘッダ部で次に続くものはTPCヘッダであることが明らかになっているので、次はTCPヘッダとして解析を行う。
まず先頭2バイトが送信元ポート番号を、続く2バイトが送信先ポート番号を表している。
今回は4134番ポートから443番ポートまでの通信であることが分かった。
なお443番ポートはSSL通信に使われるポートである。
続いての4バイトはこのデータが何番目かを表すシーケンス番号を示していて、今回は1から28までとなっている。
次の4バイトが応答確認番号で、今回は1番目のデータまで受信できたことを示す。
続く4bitはtpcヘッダ長で、今回は「4バイト * 5個 = 20バイト」あることが示されている。
その次の6bitが予約領域となっているが今回は0で埋められている。
続く6bitが種々のフラグを表しているが、今回は「Acknowledge」と「Push」にフラグが立っていることが分かる。
これはそれぞれ有効なACKがTCPヘッダ内に存在していることと、
受信したデータを直ちにこのパケットを必要とするアプリに引き継ぐことを示している。
続いての2バイトはウィンドウサイズを示している。今回は252となっており、
パケットをどれほど平行して送って良いかどうかの確認に使われる。
次の2バイトがチェックサムを示しており、このチェックサムもWiresharkに
計算させたところ正しいヘッダが送られてきていることが分かった。
最後の2バイトは緊急ポインタとなっているが、今回は緊急データに関するフラグが立っていないので、0で埋められている。

TCPヘッダの分析によって続くデータはSSL通信である事が判明している。
まず先頭1バイトがSSL通信で何が運ばれているのかが判別できる。
今回は24番が指定されており、調べたところHeartbeatという、
システムが一定時間ごとに応答を出すことによってそれが動作しているかどうかを
確認するために使われるものであると分かった。
特に今回はSSLの状態を確認するためのSSL Heartbeatとなっている。
続く2バイトはSSL通信のバージョンを示していて、今回はTLS1.2が使われていることが分かった。
このバイナリ中では「0x0303」となっているが、これがバージョン番号と対応している。
次の2バイトがそこに続く実際のデータの長さを表している。今回は23バイトある。

次に続く部分はHeatbeatに関するデータとなっている。
まず先頭1バイトにある「1」は、送信先サーバーにデータを要求する事を示している。
続いての2バイトに要求するペイロード長が書き込まれている。今回は3835バイト分を要求している。
一方で、実際に送信したペイロードがそれに続く4バイトに書き込まれている。
もし、このパケットがHeartbleed脆弱性(CVE-2014-0160)のあるOpenSSLに送信されてしまうと、
実際に送信したペイロード以上のデータがサーバーのメモリ上から返ってきてしまう。
これによってもしメモリ上に秘密鍵や、通信したユーザーID・パスワードが残っていた場合、
これが送信されてしまうこととなるだろう。実際のペイロードの後に続く16バイト部分に
あるものは自分の知識では具体的にどんなデータであるか分析することはできなかったが、
おそらく、攻撃に必要な部分が埋め込まれているものだと推測される。

【回答ここまで】