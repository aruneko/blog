+++
date = "2016-07-24T12:28:39+09:00"
draft = false
title = "2016年度LOCAL総会に参加してきた"
tags  = ["LOCAL"]
+++

LOCAL学生部部長として、そして正会員の一人として[一般社団法人LOCAL](http://www.local.or.jp/)の総会に参加してきました。

議案に従って淡々と議決を行い、スムーズに進行したのではないかと思います。

そのほかに昨年の活動を通して気になった点について詳しい議論を行いました。

学生視点からの提案もできたと思います。その結果学生会員になりやすくなったので、大学生だけでなくもっと若い中学生や高校生の皆さんにも気軽に会員になって、一緒に活動して行けたらと思います。

そんなやる気のある若い学生さんは[学生部のサイト](http://students.local.ro.jp/)を見つつ、遠慮無く僕などに声をかけていただければと思います。

もちろん、正会員でなくてもOKなので！
