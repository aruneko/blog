+++
date = "2016-06-18T15:28:39+09:00"
draft = false
title = "[OSC2016Hokkaido]参加レポート"
tags  = ["OSC"]
+++

今年も開催されましたOSC2016 Hokkaido。遅くなってしまいましたが、レポートしていきましょう。

今年はレポートスタッフと、LOCAL学生部ブースと、セキュリティキャンプブースの3役を兼ねた大忙しのOSCとなりました。

いつも通りギリギリ間に合わない時間に到着して展示会場の準備を行いました。

展示会場では学生部ブースとキャンプブースの2種類のブースをさくっと準備し、お客さんを待ち構えるなどしておりました。

学生部ブースでは今回のために印刷した学生部紹介カードと、去年印刷したステッカーを準備し、やってきた学生さんなどに活動の紹介をしながら隣のキャンプブースへの誘導もちゃっかり行っていました。

キャンプブースでは、ぱろっく師匠謹製の常設型CTFを展示しつつ、隣の学生部ブースから横流しした学生さんたちにキャンプについて説明して、是非とも応募してくださいねーと呼びかけました。

そんなこんなで忙しかったために見たセミナーは少なめでした。キャンプブースでご一緒したすらんくさんのパケット解析セミナーと、VSCodeを使ってTypescriptを書いてみよう的なセミナーぐらいしか見る暇が無かったのがちょっと心残りな点です。

お昼ご飯は新たな名物スタッフカレーで、柔らかく煮込まれた牛肉がとってもおいしかったです。

そして今年は宿も確保できたため、2011年の初参加以来一度も行けなかった閉会式と懇親会にも出席することができました。

閉会式LTの盛り上がり方はそれはそれはもうたいへんなもので、席に座れない人が通路にあふれかえっていました。

僕のお友達もLTをするというので大変楽しみにしていましたが、やはり期待を裏切らずレベルの高いLTを披露してくれました。

その後懇親会へ向かい、ジンギスカンを楽しみつつ、キャンプに行くためにはどうすればいいか的な話を学生さんにしつつ、という楽しい時間を過ごすことができました。某2人組によるコント「ビールかけ」は大変楽しませていただきました(?)。

ちゃっかり[懇親会LT](http://www.slideshare.net/aruneko99/osc16do-lt)も行ったりしまして、意外とウケたのでよしとします。

続く2次会でもカルーアミルクを片手にいろんな大人の人とおしゃべりすることができて、勉強になりました。さくらインターネットの方とMinecraftな話題で盛り上がれたのも楽しかったです。

無事に終わったOSC16doですが、僕が執筆を担当した[公式レポート](http://www.ospn.jp/press/20160708osc2016-hokkaido-report.html)が公開されていますので、そちらもご覧になっていただければと思います。

それではまた来年！
